## Forenote
I wrote all of this document up in a different csgitlab project and then copied and pasted it in here as it belongs to this project. 


## Lab 1 Practical
I an writting this document to follow along with the exercise set out under 'Excercise Week 1'
which I think is the lab exercise. I will be writting my assumptions and discoveries as I go along

`1. mkdir -p $HOME/portfolio/week1 ; cd $HOME/portfolio/week1`

`2. cd ~`

### Expectations:
I think this will attempt to create a folder called portfolio and a subfolder called week1.
Then it will try to change into the directory and then change back to the home page. I however
think it will cause an error as I'm not sure this will organise the files nicely in accordance
to my organisation scheme because I am working in a folder called labs/lab_1 so this will bounce
my work around.
### Findings:
Correct so far, it created a folder and a subfolder and changed me into them.
the ; character treats the two commands as seperate lines I think but I have no idea what
the -p does. cd ~ changes the directory to the base directory. I'm going to add and push this
to gitlab after every attempt including now.

`3. rm -r portfolio`

`4. mkdir -p $HOME/portfolio/week1 & cd $HOME/portfolio/week1`

`5. cd ~`

### Expectations:
3 will remove the portfolio folder and every sub folder/file (the -r means recursive), 4 will
remake the same directory and change to it hopefully, not sure what & means , probably AND but
not sure, also forgot to say that $HOME directory is the top level directory. 5. will just
change directory back to the top.

### Findings:
It actually gave me an error saying that the folder does not exist. It appears to have tried to
accomplish either both code lines at once or in reverse order. Quite perculiar.

`6. rm -r portfolio`
`7. mkdir -p $HOME/portfolio/week1 && cd $HOME/portfolio/week1`
`8. echo "Hello World"`
`9. echo Hello, World`
`10. echo Hello, World; Foo bar`

### Expectations:
6 is classic and will remove the directories again, 7 will remake it and I assume it'll work, 
still unsure what && means but that probably definitely means and, I also assume that the other
command I did earlier with & only didn't work because I mistyped something. I assume
8 will return Helo World onto the screen as well as 9 which won't break because the comma will
be seen as a concatenation operator but I'm unsure on that one. and by the same logic
10 will work and print hello, world but foo bar, acting as a second command will break.

### Findings:
All good so far actually :)

`11. echo Hello, world!`
`12. echo "line One"; "line two"`
`13. echo "Hello, World > readme"`
`14. echo "Hello, world" > readme `
`15. cat readme

### Expectations:
11 will just echo "Hello, world!" onto the screen, 12 will echo "line one" on one line and "line 
two" on the line below it. 13 will echo exactly what is in the speech marks but 14 will not, 
instead it'll create a file called readme and put "Hello, World" into it. 15 will then read 
out the file.

### Findings:
I was correct about 11 but 12 caught me out, I should've noticed that there's no echo command 
after the ;, therefore it echoed "line one" and then gave an error "line two" is not a command.
13 I was right about, and 14, where the > symbol meant kind of like "put in here", as it made
a file called readme and unsurprsingly 15 concatenated the contents of readme onto the screen.

`16. example="Hello,World"`
`17. echo $example`
`18. echo '$example'`
`19. echo "$example"`
`20. echo "please enter your name"; read example`
`21. echo "Hello $example"`

### Expectations:
16 Will create a variable named example with the data "hello,world" on it.
17 will echo out the variable's data.
18 will echo $example. 
19 will also do the same thing just written differently.
20 will wait for the user's input after asking a question and make the user's input the variable
example.
21 will print out "hello" followed by whatever the user input.

### Findings:
16 did infact set a variable
17 correct, it output the variable value
18 correct it output literally '$example'
19 incorrect, it actually still output the variable,
20 it infact did wait for input and store my value
21 it returned my value as output part of the pgrase

`22. three=1+1+1;echo $three`
`23. bc`


### Expectations:
22: create a variable called three that has the integer value 3, and then print it out.
23: Honestly no idea what bc will do 

### Findings
22 actually I WAS WRONG, 1+1+1 was taken as a string and so three="1+1+1"
23 It returned the result 0. I then searched it so found that bc means basic calculator. Very cool.

`24. echo 1+1+1 | bc`
`25. let three=1+1+1;echo $three`
`26. echo date`

### Expectations:
24:Will print 1+1+1 and then put it into the basic calculator, returning 3
25: Will define three mathematically and so will work out the sum instead of taking it as a string and then print it.
26: Will print the date.

### Findings:
24 Correct
25 Correct again
26 nope, it just printed date, I feel that this was a trick put in to crush our expectations.

### New procedure
From now on as the commands are getting too hard to predict all at a time, I will simply be doing analysis on each line one at a time.

`27. cal`
Expectation: it will open up calculator. Findings: It opens up a cute calendar !!!

`28. which cal`
Expectation: It tells you details about calendar. Findings: it gives you the file path ( i think ) of the program.

`29. /bin/cal`
Expectation: It will run the calendar command again, this time calling it from where it actually lives. Findings: YES!

`30. $(which cal)`
Expectations: Honestly not sure. Findings: It just ran the command.

`31. 'which cal'`
Expectations: it will just process it as a random string and ignore it because it won't know what 'which means or something. Findings: Says the command isn't found so yeah

`32. echo "The date is $(date)"`
Expectations: maybe it'll return the time. Findings: Yes it did.

`33. seq 0 9`
expectations: It will print numbers from 0 to 9 in order. Findings: Yes, and one number per line too.

`34 seq 0 9 | wc -l`
expectations: for each number 0 to 9 it will put it into the wc function, not sure what it is yet. Findings: it printed 10, it stood for word count. and there were 10 "words".

`35. seq 0 9 > sequence`
Expectations: no clue what sequence will actually do. Findings: did nothing, maybe it stored it in sequence

`36. wc -l < sequence`
Expectations: going on my previous hypothesis, it will put the contents of sequence into the word count command. Findings: I think that's what it did, it returned 10 as expected.

`37. for i in $(seq 1 9); do echo $i; done`
Expecations: a rudimentary for loop that will print each number, not sure what done does at the end. Findings: correct, and if you don't put the done at the end it will keep waiting for your input so I think done acts as a way to say you've finished writing code.

`38. (echo -n 0; for i in $(seq 1 9) ; do echo -n +$i; done; echo) | bc`
Expectations: it will print 1+2+3+..+8+9 and then feed that to the basic calculator which will return whatever the sum is. The -n meaning it won't do it on a new line Findings: Yes, it printed the output 45 that's so nice of it.

`39. echo -e '#include <stdio.h\nint main(void)\n{printf("Hello World\\n");\n return 0;\n}>hello.c`
Expectations: it will create a program with the above text (which makes a c program) and puts it in hello.c
Findings:It has indeed created a program called hello.c and accessing it with nano revealed our program.

`40. cat hello.c`
Expectations: prints out file contents.
Findings: yes

`41. gcc hello.c -o hello`
Expectations: compiles the program into a file called hello
Findings: Yep

`42. ./hello`
Expectations: runs the program, outputting "hello world"
findings: Incredible! it has done it!!
